# pylint: disable=invalid-name
""" Extracts saved classifier results and performs an averaging """

import argparse
import os

import numpy as np
import glob

parser = argparse.ArgumentParser()

parser.add_argument('--folder', '-f', help='Folder to analyze.')

if __name__ == '__main__':
    args = parser.parse_args()

    my_folder = os.path.join(args.folder, '**/*.npy')
    available_files = glob.glob(my_folder, recursive=True)

    eplus_results = {}
    gamma_results = {}
    piplus_results = {}
    
    for idx, entry in enumerate(available_files):
        identifier = ''
        if 'no_normalize' in entry:
            identifier += 'no_normalize_'
        elif 'normalize' in entry:
            identifier += 'normalize_'
        else:
            raise ValueError('Are results normalized?')
        if 'no_logit' in entry:
            identifier += 'no_logit_'
        elif 'logit' in entry:
            identifier += 'logit_'
        else:
            raise ValueError('Are results in logit space?')
        if 'DNN' in entry:
            identifier += 'DNN'
        elif 'CNN' in entry:
            identifier += 'CNN'
        else:
            raise ValueError('Was a DNN or CNN used?')
        loaded_file = np.load(entry)[None, ...]
        if 'eplus' in entry:
            if identifier not in eplus_results:
                eplus_results[identifier] = loaded_file
            else:
                if len(loaded_file[0]) == 10:
                    eplus_results[identifier] = np.concatenate(
                        [eplus_results[identifier], loaded_file])
                else:
                    eplus_results[identifier+'_short'] = loaded_file
        elif 'gamma' in entry:
            if identifier not in gamma_results:
                gamma_results[identifier] = loaded_file
            else:
                if len(loaded_file[0]) == 10:
                    gamma_results[identifier] = np.concatenate(
                        [gamma_results[identifier], loaded_file])
                else:
                    gamma_results[identifier+'_short'] = loaded_file
        elif 'piplus' in entry:
            if identifier not in piplus_results:
                piplus_results[identifier] = loaded_file
            else:
                if len(loaded_file[0]) == 10:
                    piplus_results[identifier] = np.concatenate(
                        [piplus_results[identifier], loaded_file])
                else:
                    piplus_results[identifier+'_short'] = loaded_file
        else:
            raise ValueError('Which particle was analyzed?')
        #print(idx+1, entry)

    # acc, auc, jsd, best_epoch
    output_text = "{} = {} +/- {}"
    print('eplus:')
    for entry in eplus_results.keys():
        print(entry)
        arr = eplus_results[entry]
        print("Based on {} different samples:".format(len(arr)))
        means = arr.reshape(-1, 4).mean(0)
        stds = arr.reshape(-1, 4).std(0)
        print("Overall: ")
        print(output_text.format('Accuracy', means[0], stds[0]))
        print(output_text.format('AUC', means[1], stds[1]))
        print(output_text.format('JSD', means[2], stds[2]))
        print("Based on epochs between {} and {}".format(arr.reshape(-1, 4).min(0)[3],
                                                         arr.reshape(-1, 4).max(0)[3]))
        if False:
            print("averaged over samples first:")
            sample = arr.mean(0)
            samplestd = arr.std(0)
            sample_mean = sample.mean(0)
            sample_std = sample.std(0)
            for idx, sam in enumerate(sample):
                print('Individual results:', idx+1)
                print(output_text.format('Accuracy', sam[0], samplestd[idx, 0]))
                print(output_text.format('AUC', sam[1], samplestd[idx, 1]))
                print(output_text.format('JSD', sam[2], samplestd[idx, 2]))
            print('average:')
            print(output_text.format('Accuracy', sample_mean[0], sample_std[0]))
            print(output_text.format('AUC', sample_mean[1], sample_std[1]))
            print(output_text.format('JSD', sample_mean[2], sample_std[2]))
        print(' ')
    print('gamma:')
    for entry in gamma_results.keys():
        print(entry)
        arr = gamma_results[entry]
        print("Based on {} different samples:".format(len(arr)))
        means = arr.reshape(-1, 4).mean(0)
        stds = arr.reshape(-1, 4).std(0)
        print("Overall: ")
        print(output_text.format('Accuracy', means[0], stds[0]))
        print(output_text.format('AUC', means[1], stds[1]))
        print(output_text.format('JSD', means[2], stds[2]))
        print("Based on epochs between {} and {}".format(arr.reshape(-1, 4).min(0)[3],
                                                         arr.reshape(-1, 4).max(0)[3]))
        if False:
            print("averaged over samples first:")
            sample = arr.mean(0)
            samplestd = arr.std(0)
            sample_mean = sample.mean(0)
            sample_std = sample.std(0)
            for idx, sam in enumerate(sample):
                print('Individual results:', idx+1)
                print(output_text.format('Accuracy', sam[0], samplestd[idx, 0]))
                print(output_text.format('AUC', sam[1], samplestd[idx, 1]))
                print(output_text.format('JSD', sam[2], samplestd[idx, 2]))
            print('average:')
            print(output_text.format('Accuracy', sample_mean[0], sample_std[0]))
            print(output_text.format('AUC', sample_mean[1], sample_std[1]))
            print(output_text.format('JSD', sample_mean[2], sample_std[2]))
        print(' ')
    print('piplus:')
    for entry in piplus_results.keys():
        print(entry)
        arr = piplus_results[entry]
        print("Based on {} different samples:".format(len(arr)))
        means = arr.reshape(-1, 4).mean(0)
        stds = arr.reshape(-1, 4).std(0)
        print("Overall: ")
        print(output_text.format('Accuracy', means[0], stds[0]))
        print(output_text.format('AUC', means[1], stds[1]))
        print(output_text.format('JSD', means[2], stds[2]))
        print("Based on epochs between {} and {}".format(arr.reshape(-1, 4).min(0)[3],
                                                         arr.reshape(-1, 4).max(0)[3]))
        if False:
            print("averaged over samples first:")
            sample = arr.mean(0)
            samplestd = arr.std(0)
            sample_mean = sample.mean(0)
            sample_std = sample.std(0)
            for idx, sam in enumerate(sample):
                print('Individual results:', idx+1)
                print(output_text.format('Accuracy', sam[0], samplestd[idx, 0]))
                print(output_text.format('AUC', sam[1], samplestd[idx, 1]))
                print(output_text.format('JSD', sam[2], samplestd[idx, 2]))
            print('average:')
            print(output_text.format('Accuracy', sample_mean[0], sample_std[0]))
            print(output_text.format('AUC', sample_mean[1], sample_std[1]))
            print(output_text.format('JSD', sample_mean[2], sample_std[2]))
        print(' ')
