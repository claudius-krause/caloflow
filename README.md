# CaloFlow: Fast and Accurate Generation of Calorimeter Showers with Normalizing Flows
## by Claudius Krause and David Shih

This repository contains the source code for reproducing the results of

_"CaloFlow: Fast and Accurate Generation of Calorimeter Showers with Normalizing Flows"_ by Claudius Krause and David Shih, [arXiv:2106.05285](https://arxiv.org/abs/2106.05285), [Phys. Rev. D 107 (2023), 113003](https://doi.org/10.1103/PhysRevD.107.113003)

and

_"CaloFlow II: Even Faster and Still Accurate Generation of Calorimeter Showers with Normalizing Flows"_ by Claudius Krause and David Shih, [arXiv:2110.11377](https://arxiv.org/abs/2110.11377), [Phys. Rev. D 107 (2023), 113004](https://doi.org/10.1103/PhysRevD.107.113004)

If you use the code, please cite
```
@article{Krause:2021ilc,
    author = "Krause, Claudius and Shih, David",
    title = "{Fast and accurate simulations of calorimeter showers with normalizing flows}",
    eprint = "2106.05285",
    archivePrefix = "arXiv",
    primaryClass = "physics.ins-det",
    doi = "10.1103/PhysRevD.107.113003",
    journal = "Phys. Rev. D",
    volume = "107",
    number = "11",
    pages = "113003",
    year = "2023"
}

@article{Krause:2021wez,
    author = "Krause, Claudius and Shih, David",
    title = "{Accelerating accurate simulations of calorimeter showers with normalizing flows and probability density distillation}",
    eprint = "2110.11377",
    archivePrefix = "arXiv",
    primaryClass = "physics.ins-det",
    doi = "10.1103/PhysRevD.107.113004",
    journal = "Phys. Rev. D",
    volume = "107",
    number = "11",
    pages = "113004",
    year = "2023"
}
```

### Detector Layout and Training Data
We consider the same toy detector that the original [CaloGAN](https://arxiv.org/abs/1712.10321) used. It is a three-layer, liquid Argon (LAr) electromagnetic calorimeter (ECal) cube with 480mm side length. The three layers have a voxel resolution of $`3\times 96`$, $`12\times 12`$, and $`12\times 6`$, respectively. 

We use GEANT4-based training data similar to the original one of [CaloGAN](https://arxiv.org/abs/1712.10321). The reason we generated a new dataset is that we needed more samples for the further evaluation of the normalizing flow with a neural classifier. **Our training data can be found at [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.5904188.svg)](https://doi.org/10.5281/zenodo.5904188).** The original data of [CaloGAN](https://arxiv.org/abs/1712.10321) is also available and can be found at [![DOI](https://zenodo.org/badge/DOI/10.17632/pvn3xc3wy5.1.svg)](https://doi.org/10.17632/pvn3xc3wy5.1).
To speed up the plotting of reference data, we store the relavant features in separate .hdf files ("plots_reference_xx.hdf", with xx the applied threshold in MeV). We obtained the CaloGAN reference by training our own GAN based on the source code available at [![DOI](https://zenodo.org/badge/82329392.svg)](https://zenodo.org/badge/latestdoi/82329392).

Running `python split_hdf5.py --file=eplus.hdf5` from the folder hdf5-helper/ splits the original hdf5 file of CaloGAN in two files with a training and testing set. 

### Running CaloFlow
CaloFlow uses the [pytorch](https://pytorch.org/) implementation of Normalizing Flows called [nflows](https://github.com/bayesiains/nflows).

#### CaloFlow v1 (teacher)
To train CaloFlow v1 as in the first paper, run

`python run.py -p=eplus --train --data_dir=path/to/GEANT/data/ --output_dir=./results/`

To generate a sample of 100k events to file, run
`python run.py -p=eplus --generate_to_file`

To evaluate the NLL of the testset, run
`python run.py -p=eplus --evaluate --data_dir=path/to/GEANT/data/`

Samples for gamma and piplus can be generated with the options `-p=gamma` and `-p=piplus` respectively. One can also specify to generate a sample that is only plotted and not saved in a separate file using `--generate`. For loading a saved flow I, add `--flowI_restore_file=path/to/weights.pt` to the call. For loading a saved flow II, add `--restore_file=path/to/weights.pt` to the call. Per default, the code will look for the saved weights at `output_dir/saved_checkpoints/Flow_{I or II}/`. During training, the full model with all weights, but also with the full optimizer state is saved. Calling `run.py` with the option `--save_only_weights` loads such a saved model of flow II and saves only the weights and not the optimizer state.

The saved weights that were used for the publication can be found at [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.7849133.svg)](https://doi.org/10.5281/zenodo.7849133).

#### CaloFlow v2 (student)
To train CaloFlow v2 as in the second paper, run

`python run.py -p=eplus --student_mode --train --data_dir=path/to/GEANT/data/ --output_dir=./results/`

To generate a sample of 100k events to file, run
`python run.py -p=eplus --student_mode --generate_to_file`

To evaluate the NLL and the KL of the testset, run
`python run.py -p=eplus --student_mode --evaluate --evaluate_KL --data_dir=path/to/GEANT/data/`

Samples for gamma and piplus can be generated with the options `-p=gamma` and `-p=piplus` respectively. One can also specify to generate a sample that is only plotted and not saved in a separate file using `--generate`. For loading a saved flow I, add `--flowI_restore_file=path/to/weights.pt` to the call. For loading a saved flow II, add `--restore_file=path/to/weights.pt` to the call. Per default, the code will look for the saved weights at `output_dir/saved_checkpoints/Flow_{I or II}/`. During training, the full model with all weights, but also with the full optimizer state is saved. Calling `run.py` with the option `--save_only_weights` loads such a saved model of flow II and saves only the weights and not the optimizer state.

The saved weights that were used for the publication can be found at [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.7849133.svg)](https://doi.org/10.5281/zenodo.7849133).

Additional options for `run.py` are listed at the bottom of the page.

### Running the Classifier

First, the correct dataset files need to be created. This is done in two steps:

1. The file containing the generated showers (assuming it contains 100k events) needs to be split i nto train/test/val sets. This can be done with the script [split_classifier_hdf5.py](hdf5-helper/split_classifier_hdf5.py) and the command `python split_classifier_hdf5.py --file=file_to_be_split.hdf5`. The created files `train_cls_file_to_be_split.hdf5`, `test_cls_file_to_be_split.hdf5`, and `val_cls_file_to_be_split.hdf5` will now contain 60k/20k/20k events, respectively.

2. The dataset files containing both the GEANT4 and sampled data must be created. This can be done with the script [merge_hdf5.py](hdf5-helper/merge_hdf5.py) and the command
`pyhton merge_hdf5.py --file1=file_with_truth_label_0.hdf5 -- file2=file_with_truth_label_1.hdf5`. The labels will be added when the merged file is created.

Then run 

`python classify.py -p=filename_of_merged_dataset_without_extension --data_dir=path/to/GEANT/data/ --save_dir=./classifier/  --mode=["DNN-low", "DNN-high", or "CNN"]`

to start training the classifier. Normalized calorimeter layer data can be used with `--normalize` and `--use_logit` transforms data into logit space before training. 

You can also automate the running of 10 classifier runs with subsequent averaging of results with the shell script

```
#!/bin/bash                                                                     

FOLDER="my_output_folder"
MODE="DNN-low"

mkdir -p $FOLDER
for i in {1..10}
do
    echo "Running Classifier $i in Folder $FOLDER"
    python classify.py -p=filename_of_merged_dataset_without_extension --data_dir=path/to/GEANT/data/ --save_dir=$FOLDER --mode=$MODE -r=$i
done

FINAL_CODE=$(cat <<END                                                          
import numpy as np                                                              
arr = np.load('$FOLDER'+'/summary_'+'$MODE'+'.npy', allow_pickle=True)          
arr_mean = arr.mean(axis=0)                                                     
arr_std = arr.std(axis=0)


print("Results of 10 runs:")                                                    
print("AUC = {} +/- {}".format(arr_mean[1], arr_std[1]))                        
print("JSD = {} +/- {}".format(arr_mean[2], arr_std[2]))                        
print("Based on epochs ", arr[:, -1])                                           
                                                                                
END                                                                             
)
res="$(python -c "$FINAL_CODE")"
echo "$res"
echo "$FOLDER"

```

Additional options for `classify.py` are listed at the bottom of the page.

### Plotting Results
The generated showers in the .hdf5 files can be evaluated using histograms. 

- Option 1: plot one histogram per file. This can be done with the script `plot_my_calo.py`, but only for one sample at a time.  `python plot_my_calo.py -f path/to/showers.hdf5 -r path/to/results_folder --include_GEANT --include_CaloGAN`. The latter two flags will also include the GEANT4 and CaloGAN reference in the histograms. 

- Option 2: plot collection of histograms per file.
    - To reproduce the figures of the CaloFlow paper 1, run `python plot_all_particles.py --eplus_file path/to/eplus_showers.hdf5 -r path/to/output_folder --plot_GAN --no_legend` if you wish to plot $e^{+}$ showers. Other shower types can be included with `--gamma_file` or `--piplus_file`, also at the same time. The option `--plot_GAN` includes the CaloGAN reference, `--no_legend` suppresses the legends in the produced plots.
    - To reproduce the figures of the CaloFlow paper 2, run `python plot_all_particles.py --eplus_file path/to/eplus_showers.hdf5 --eplus_student_file path/to/eplus_student_showers.hdf5 -r path/to/output_folder --no_legend` 


### Additional options

- `--help` lists all available options and a short description of them.

#### common to `run.py` and `classify.py`

- `--no_cuda` allows the user to not use the GPU. 

- `--which_cuda` allows the user to specify on which of the possibly multiple available GPUs to run.

- `--log_interval` Specifies after how many batches the loss statistics are printed in training. Defaults to 175, which with the default dataset size and batch size results to twice per epoch.

- `--dropout_probability` Specifies the dropout applied to the NN. Defaults to 0.05 in Flow II and 0 in Flow I (fixed) and the classifier. 

- `--batch_size` Batch size used in training. Defaults to 200 in `run.py` and 1000 in `classify.py`. 

- `--n_epochs` Number of epochs to train for. Defaults to the values reported in the publications (100 for the Flows, 150 for the classifiers). 

- `--lr` Learning rate. Defaults to the values reported in the publications. 

#### `run.py`

- `--output_dir` specifies the folder in which the output is saved. Defaults to `./results`.

- `--results_file` specifies the filename in which all terminal output is saved. It will automatically be moved to `output_dir`. Defaults to `results.txt`. 

- `--log_preprocess` Switches to using a simple `torch.log10` preprocessing instead of the vector $u$, which is introduced in CaloFlow v1, equation 3. Not used for the publications here. 

- `--with_noise` Adds noise to the training data, is enabled per default.

- `--threshold`: Sets the threshold (in MeV) that is applied to the sample after generation as part of the post-processing. Defaults to 0.01. 

- `--save_every_epoch` saves the student weights every epoch, not only when the KL is smaller than the previously saved KL.

- `--n_blocks` Number of MADE blocks in the teacher flow. Defaults to 8, as in the publication.

- `--hidden_size` Hidden layer size inside each teacher MADE block.

- `--n_hidden` Number of hidden layers (in `nflows` convention) in MADE block. Defaults to 1.

- `--activation_fn` Which activation function to use inside the MADE blocks. Defaults to ReLU.

- `--n_bins` Number of bins that the RQS is made of. Defaults to 8, as in the publications.

- `--tail_bound` Sets the domain of the RQS. Defaults to 14, as required by the internal parameter ALPHA. 

- `--student_n_blocks` Number of MADE blocks in the student flow. Defaults to 8, as in the publication.

- `--student_hidden_size` Hidden layer size inside each student MADE block.

- `--student_width` Width of the Gaussian base distribution that is used for z-pass student training.

- `--beta` Sets the relative weight between z-loss (beta=0) and x-loss (beta=1). Defaults to 0.5 as in the publication.

- `--fully_guided` Uses the fully guided loss in student training. Enabled per default, as it was used for the publication.

- `--train_xz` Uses the MSE-loss in x and z pass between the individual MADE blocks. Is set to `True` when `--fully_guided` is used. 

- `--train_p` Uses the MSE-loss in x and z pass between the outputs of individual MADE blocks (RQS parameters). Is set to `True` when `--fully_guided` is used.

#### `classify.py`

- `--n_layer` Number of hidden layers in the classifier. Defaults to 2, as in the publications.

- `--n_hidden` Number of neurons in the hidden layers. Defaults to 512, as in the publications. 

- `--threshold` If threshold of 1e-2MeV is applied to data before training.

- `--load` If used, a saved model state from `--save_dir` is loaded instead of training. 

#### `plot_my_calo.py` and `plot_all_particles.py`

- `--data_thres` can be used to add a threshold (in MeV) to data before creating the histogram.

- `--GEANT_thres` can be used to add a threshold (in MeV) to the GEANT4 reference before creating the histogram.

