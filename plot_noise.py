# pylint: disable=invalid-name
""" Reads in loss curves from .npy files and calorimeter data from .hdf5 files
    with the goal to show the influece of noise regularization.

    This code was used for the following publications:

    "CaloFlow: Fast and Accurate Generation of Calorimeter Showers with Normalizing Flows"
    by Claudius Krause and David Shih
    arxiv:2106.05285, Phys.Rev.D 107 (2023) 11, 113003

    "CaloFlow II: Even Faster and Still Accurate Generation of Calorimeter Showers with
     Normalizing Flows"
    by Claudius Krause and David Shih
    arXiv:2110.11377, Phys.Rev.D 107 (2023) 11, 113004

"""

import argparse
import os

import numpy as np
import pandas as pd
import torch
import matplotlib
import matplotlib.pyplot as plt

import plot_calo
from data import CaloDataset


parser = argparse.ArgumentParser()

parser.add_argument('--noise_folder', '-n', help='path/to/noise/folder/')
parser.add_argument('--noise_hdf5', help='path/to/noise/file.hdf5')
parser.add_argument('--plain_folder', '-p', help='path/to/plain/folder/')
parser.add_argument('--plain_hdf5', help='path/to/plain/file.hdf5')
parser.add_argument('--particle_type', '-t', default='piplus', help='["eplus", "gamma", "piplus"]')
parser.add_argument('--results_dir', '-r', help='path/to/results_folder')

parser.add_argument('--data_thres', type=float, default=0.,
                    help='Threshold in MeV to apply to CaloFlow data')
parser.add_argument('--GEANT_thres', type=float, default=0.,
                    help='Threshold in MeV to apply to GEANT data')

def plot_loss(noise_train, noise_test, plain_train, plain_test, fp, single=True, save_it=True,
              particle='eplus'):
    """ plots the training and test loss of a noise and and plain run
        train_loss: list of losses obtained during training (datasize/batchsize per epoch)
        test_loss: list of losses from the test set (one per epoch)
        fp: file path to save file
        save_it: bool, whether or not to save (or just show) result
        particle: str, which particle is plotted
    """
    # test_loss is written once per epoch: gives number of epochs to plot
    noise_num_epochs = len(noise_test)
    noise_num_batches = len(noise_train) / noise_num_epochs
    plain_num_epochs = len(plain_test)
    plain_num_batches = len(plain_train) / plain_num_epochs
    if single:
        plt.figure(figsize=(8, 8))
    else:
        fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True, figsize=[8, 8])
        fig.subplots_adjust(hspace=0.15)

    plt.xlim([0., np.max([plain_num_epochs, noise_num_epochs])])
    ylim = np.min([np.min(noise_train),
                   np.min(noise_test),
                   np.min(plain_train),
                   np.min(plain_test)])

    noise_test_arg = np.arange(1., noise_num_epochs+1, 1)
    noise_train_arg = np.arange(0., noise_num_epochs, 1./noise_num_batches) + (1./noise_num_batches)
    plain_test_arg = np.arange(1., plain_num_epochs+1, 1)
    plain_train_arg = np.arange(0., plain_num_epochs, 1./plain_num_batches) + (1./plain_num_batches)

    if single:
        plt.plot(noise_train_arg, noise_train, 'cornflowerblue', label='training with noise',
                 lw=.75)
        plt.plot(noise_test_arg, noise_test, 'blue', label='test with noise', lw=2.)
        plt.plot(plain_train_arg, plain_train, 'lightcoral', label='training without noise', lw=.75)
        plt.plot(plain_test_arg, plain_test, 'red', label='test without noise', lw=2.)
        plt.ylim([ylim, 1000.])
        plt.ylabel('loss')
        plt.legend(loc='right', fontsize=18)
    else:
        ax1.plot(noise_train_arg, noise_train, 'cornflowerblue', label='training with noise',
                 lw=.75)
        ax1.plot(noise_test_arg, noise_test, 'blue', label='test with noise', lw=2.)
        ax1.plot(plain_train_arg, plain_train, 'lightcoral', label='training without noise', lw=.75)
        ax1.plot(plain_test_arg, plain_test, 'red', label='test without noise', lw=2.)
        ax2.plot(plain_train_arg, plain_train, 'lightcoral', label='training without noise', lw=.75)
        ax2.plot(plain_test_arg, plain_test, 'red', label='test without noise', lw=2.)
        if particle == 'piplus':
            ax1.set_ylim(550., 1e3)
            ax2.set_ylim(ylim, -800.)
        #elif particle == 'eplus':
        else:
            ax1.set_ylim(100., 300.)
            ax2.set_ylim(ylim, -800.)
        ax1.legend(loc='upper right', fontsize=18) #bbox_to_anchor=(0.01, 1.01), ncol=2


        # hide the spines between ax and ax2
        ax1.spines['bottom'].set_visible(False)
        ax2.spines['top'].set_visible(False)
        ax1.xaxis.tick_top()
        ax1.yaxis.set_ticks_position('both')
        ax1.tick_params(labeltop=False)  # don't put tick labels at the top
        ax2.xaxis.tick_bottom()
        ax2.yaxis.set_ticks_position('both')

        d = .5  # proportion of vertical to horizontal extent of the slanted line
        kwargs = dict(marker=[(-1, -d), (1, d)], markersize=12,
                      linestyle="none", color='k', mec='k', mew=1, clip_on=False)
        ax1.plot([0, 1], [0, 0], transform=ax1.transAxes, **kwargs)
        ax2.plot([0, 1], [1, 1], transform=ax2.transAxes, **kwargs)
        fig.text(0.02, 0.55, 'loss', va='center', rotation='vertical')

    plt.xlabel('epoch')

    plt.tight_layout()
    if save_it:
        plt.savefig(fp, dpi=300)
        plt.close()
    else:
        plt.show()

def plot_E_ratio_0(tensor_noise, tensor_plain, fp, save_it=True, lower_threshold=0.,
                   ref_thres=None, plot_ref=None):
    """ plots the ratio of the difference of the 2 brightest voxels to their sum,
        tensor: batch of input data (nbatch, 288), as here only layer 0 is to be plotted
        fp: file path to save file
        layer: str (0, 1, or 2) to indicate layer number
        save_it: bool, whether or not to save (or just show) result
        plot_ref: if not none, plot reference of that
                  type (eplus, gamma, piplus)
    """
    noise_data = tensor_noise.clamp_(0., 1e5).to('cpu').numpy()
    noise_data = noise_data.reshape(noise_data.shape[0], -1)
    noise_data = np.where(noise_data < lower_threshold, np.zeros_like(noise_data), noise_data)
    noise_top2 = np.sort(noise_data, axis=1)[:, -2:]
    plain_data = tensor_plain.clamp_(0., 1e5).to('cpu').numpy()
    plain_data = plain_data.reshape(plain_data.shape[0], -1)
    plain_data = np.where(plain_data < lower_threshold, np.zeros_like(plain_data), plain_data)
    plain_top2 = np.sort(plain_data, axis=1)[:, -2:]


    noise_E_ratio = ((noise_top2[:, 1] - noise_top2[:, 0]) / (noise_top2[:, 0] + noise_top2[:, 1]))\
        [noise_top2[:, 1] > 0]
    plain_E_ratio = ((plain_top2[:, 1] - plain_top2[:, 0]) / (plain_top2[:, 0] + plain_top2[:, 1]))\
        [plain_top2[:, 1] > 0]

    bins = np.linspace(0, 1, 100)
    axis_label = r'$E_{\mathrm{ratio},0}$'
    colors = matplotlib.cm.gnuplot2(np.linspace(0.2, 0.8, 3))
    plt.figure(figsize=(7, 7))

    if plot_ref is not None:
        if ref_thres is None:
            ref_thres = lower_threshold
        if plot_ref in ['eplus', 'gamma', 'piplus']:
            reference_data = pd.read_hdf('plots_reference_{:1.0e}.hdf'.format(ref_thres))
            reference_1 = reference_data.loc[plot_ref]['E_1_layer_0_{:1.0e}'.format(ref_thres)]
            reference_2 = reference_data.loc[plot_ref]['E_2_layer_0_{:1.0e}'.format(ref_thres)]
            reference = ((reference_1 - reference_2) / (reference_2 + reference_1))[reference_1 > 0]
            if plot_ref == 'eplus':
                color = colors[0]
                label = r'$e^+$ GEANT'
                flow_label = r'$e^+ CaloFlow$'
            elif plot_ref == 'gamma':
                color = colors[1]
                label = r'$\gamma$ GEANT'
                flow_label = r'$\gamma$ CaloFlow'
            else:
                color = colors[2]
                label = r'$\pi^+$ GEANT'
                flow_label = r'$\pi^+$ CaloFlow'
            _ = plt.hist(reference, bins=bins, histtype='stepfilled',
                         linewidth=2, alpha=0.2, density=True, color=color,
                         label=label)
        else:
            raise ValueError("'{}' is not a valid reference name".format(plot_ref))

    _ = plt.hist(plain_E_ratio, bins=bins, histtype='step', linewidth=3,
                 alpha=1, color=color, density='True',
                 label=flow_label+', no noise', ls='dashed')
    _ = plt.hist(noise_E_ratio, bins=bins, histtype='step', linewidth=3,
                 alpha=1, color=color, density='True',
                 label=flow_label+', with noise')

    plt.legend(fontsize=20)

    plt.xlabel(axis_label)

    plt.tight_layout()
    if save_it:
        plt.savefig(fp, dpi=300)
        plt.close()
    else:
        plt.show()

if __name__ == '__main__':
    matplotlib.rcParams.update({'font.size': 18})
    args = parser.parse_args()

    assert args.particle_type in ['eplus', 'gamma', 'piplus']

    if not os.path.isdir(args.results_dir):
        os.makedirs(args.results_dir)

    print("Plotting histograms for " + args.particle_type + " in folder " + args.results_dir)

    noise_train = np.load(os.path.join(args.noise_folder, 'train_loss.npy'))
    noise_test = np.load(os.path.join(args.noise_folder, 'test_loss.npy'))
    plain_train = np.load(os.path.join(args.plain_folder, 'train_loss.npy'))
    plain_test = np.load(os.path.join(args.plain_folder, 'test_loss.npy'))
    filename = 'noise_losses.png'
    plot_loss(noise_train, noise_test, plain_train, plain_test,
              os.path.join(args.results_dir, filename), save_it=True,
              single=False, particle=args.particle_type)

    noise_folder, noise_file_name = os.path.split(args.noise_hdf5)
    noise_file_name = os.path.splitext(noise_file_name)[0]
    noise_dataset = CaloDataset(noise_folder, noise_file_name, apply_logit=False)
    plain_folder, plain_file_name = os.path.split(args.plain_hdf5)
    plain_file_name = os.path.splitext(plain_file_name)[0]
    plain_dataset = CaloDataset(plain_folder, plain_file_name, apply_logit=False)

    if args.particle_type ==  'eplus':
        vmin = [3e-2, 2.5e-1, 2.5e-2]
        vmax = [3e3, 9.2e3, 1.2e1]
    elif args.particle_type == 'gamma':
        vmin = [3e-2, 2.5e-1, 3e-2]
        vmax = [2.2e3, 9.8e3, 1.7e1]
    else:
        vmin = [3.5e-2, 6.5e-1, 2e-1]
        vmax = [1.6e2, 2.1e3, 2.4e2]

    layer_0_plain = plain_dataset[:]['layer_0']*1e5
    layer_0_plain = torch.from_numpy(layer_0_plain)
    layer_0_noise = noise_dataset[:]['layer_0']*1e5
    layer_0_noise = torch.from_numpy(layer_0_noise)
    filename = 'no_noise_avg_layer_0.png'
    plot_calo.plot_average_voxel(layer_0_plain,
                                 os.path.join(args.results_dir, filename),
                                 0, vmin=vmin[0], vmax=vmax[0],
                                 lower_threshold=args.data_thres)

    filename = 'noise_e_ratio.png'
    plot_E_ratio_0(layer_0_noise, layer_0_plain,
                   os.path.join(args.results_dir, filename),
                   save_it=True, lower_threshold=args.data_thres,
                   ref_thres=args.GEANT_thres, plot_ref=args.particle_type)
